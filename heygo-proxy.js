const express = require('express');
const app = express();
const https = require('https');
const http = require('http');
const path = require('path');
const args = require('minimist')(process.argv.slice(2));
const fs = require("fs");
const router = express.Router();
const proxy = require("express-http-proxy");


const port = args.p || args.port || 443;
const mobileDebug = args.mobileDebug || false;
const SSL = !!(args.SSL);
const LOG_REQ = args.LOG_REQ;


if (SSL) {
    // Certificate
    const privateKey = fs.readFileSync('/etc/letsencrypt/live/ih1608160.vds.myihor.ru/privkey.pem', 'utf8');
    const certificate = fs.readFileSync('/etc/letsencrypt/live/ih1608160.vds.myihor.ru/cert.pem', 'utf8');
    const ca = fs.readFileSync('/etc/letsencrypt/live/ih1608160.vds.myihor.ru/chain.pem', 'utf8');

    const credentials = {
        key: privateKey,
        cert: certificate,
        ca: ca
    };

    /*Тут типо сервер стартовал*/
// Starting both http & https servers
    const httpsServer = https.createServer(credentials, app);
    httpsServer.listen(443, () => {
        console.log(`HTTPS Server running on port ${port}`);
    });

// Redirect from http port 80 to https
    http.createServer(function (req, res) {
        res.writeHead(301, { "Location": "https://" + req.headers['host'] + req.url });
        res.end();
    }).listen(80);
} else {
    app.listen(port);
    console.log(`Server started on ${port} port`);
}

app.use(express.json());
app.use("/", (req, res, next) => {
    if (LOG_REQ) {
        console.log(req.body);
    } else {
        console.log(`URL: ${req.url}`);
        console.log(`Method: ${req.method}`);
        console.log(req.body);
        console.log('=======================');
        console.log(req.headers);
        console.log('\n');
    }
    next();
});
app.use('/', proxy("https://api.heygo.school"));